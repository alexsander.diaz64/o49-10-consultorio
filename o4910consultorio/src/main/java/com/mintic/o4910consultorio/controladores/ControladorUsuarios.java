package com.mintic.o4910consultorio.controladores;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mintic.o4910consultorio.daos.UsuariosDao;
import com.mintic.o4910consultorio.models.Usuarios;

@RestController
@RequestMapping("/api")
public class ControladorUsuarios {
    
    @Autowired
    UsuariosDao usuariosDao;

    @GetMapping("/usuarios")
    public ResponseEntity<List<Usuarios>> listarUsuarios(){
        
        List<Usuarios> usuarios = new ArrayList<Usuarios>();
        usuariosDao.findAll().forEach(usuarios::add);

        if(usuarios.isEmpty()){
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
        return new ResponseEntity<>(usuarios, HttpStatus.OK);
        }
        

    }



}
