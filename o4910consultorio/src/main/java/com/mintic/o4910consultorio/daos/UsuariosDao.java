package com.mintic.o4910consultorio.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mintic.o4910consultorio.models.Usuarios;

public interface UsuariosDao extends JpaRepository<Usuarios, Long>{
    
}
