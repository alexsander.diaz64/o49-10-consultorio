package com.mintic.o4910consultorio.models;

import lombok.*;

import java.sql.*;

import javax.persistence.*;

@Getter
@Setter 
@Entity
@Table (name = "paciente")
public class Paciente {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    
    @Column (name ="idpaciente", nullable = false)
    private int idPaciente;

    @Column (name ="nombrepaciente", nullable = false)
    private String nombrePaciente;

    @Column (name ="apellidopaciente", nullable = false)
    private String apellidoPaciente;

    @Column (name ="emailpaciente", unique = true, nullable = false)
    private String emailPaciente;

    @Column (name ="fechanacimiento", nullable = false)
    private Date fechaNacimiento;

    @Column (name ="telefonopaciente", nullable = true)
    private int telefonoPaciente;
    
    @Column (name ="passwordpaciente", nullable = false)
    private String passwordPaciente;

    @Column (name ="doctorid", nullable = false)
    private int doctorId;

    public Paciente() {
    }

    public Paciente(int idPaciente, String nombrePaciente, String apellidoPaciente, Date fechaNacimiento,
            int telefonoPaciente, String emailPaciente, String passwordPaciente, int doctorId) {
        this.idPaciente = idPaciente;
        this.nombrePaciente = nombrePaciente;
        this.apellidoPaciente = apellidoPaciente;
        this.fechaNacimiento = fechaNacimiento;
        this.telefonoPaciente = telefonoPaciente;
        this.emailPaciente = emailPaciente;
        this.passwordPaciente = passwordPaciente;
        this.doctorId = doctorId;
    }

    @Override
    public String toString() {
        return "Paciente [apellidoPaciente=" + apellidoPaciente + ", doctorId=" + doctorId + ", emailPaciente="
                + emailPaciente + ", fechaNacimiento=" + fechaNacimiento + ", idPaciente=" + idPaciente
                + ", nombrePaciente=" + nombrePaciente + ", passwordPaciente=" + passwordPaciente
                + ", telefonoPaciente=" + telefonoPaciente + "]";
    }

    
}
