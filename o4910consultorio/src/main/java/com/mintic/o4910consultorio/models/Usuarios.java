package com.mintic.o4910consultorio.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Getter
@Setter
@Entity
@Table(name= "usuarios")
public class Usuarios {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    
    @Column (name = "idusuario", nullable = false)
    private int idUsuario;

    @Column (name = "emailusuario", nullable = false)
    private String emailUsuario;

    @Column (name = "passwordusuario", nullable = false)
    private String passwordUsuario;

    @Column (name = "nombreusuario", nullable = false)
    private String nombreUsuario;

    public Usuarios() {
    }

    public Usuarios(String emailUsuario, String passwordUsuario, String nombreUsuario) {
        this.emailUsuario = emailUsuario;
        this.passwordUsuario = passwordUsuario;
        this.nombreUsuario = nombreUsuario;
    }

    public int getidUsuario() {
        return idUsuario;
    }

    public void setidUsuario (int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getEmailUsuario() {
        return emailUsuario;
    }

    public void setEmailUsuario (String emailUsuario) {
        this.emailUsuario = emailUsuario;
    }

    public String getpasswordUsuario() {
        return passwordUsuario;
    }

    public void setpasswordUsuario(String passwordUsuario) {
        this.passwordUsuario = passwordUsuario;
    }
    
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario (String nombreUsuario){
        this.nombreUsuario = nombreUsuario;
    }

    @Override
    public String toString() {
        return "Usuarios [emailUsuario=" + emailUsuario + ", idUsuario=" + idUsuario + ", nombreUsuario="
                + nombreUsuario + ", passwordUsuario=" + passwordUsuario + "]";
    }

    
}