package com.mintic.o4910consultorio.models;

import java.sql.Date;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name= "cita")
public class Cita {

    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    
    @Column (name = "codigocita", nullable = false)
    private int codigoCita; 

    @Column (name = "fechacita", nullable = false)
    private Date fechaCita;

    @Column (name = "horacita", nullable = false)
    private LocalTime horaCita;

    @Column (name= "doctorid", nullable = false)
    private int doctorId;
    
    @Column (name = "pacienteid", nullable = false)
    private int pacienteId;

    public int getCodigoCita() {
        return codigoCita;
    }

    public void setCodigoCita (int codigoCita) {
        this.codigoCita = codigoCita;
    }

    public Date getFechaCiDate() {
        return fechaCita;
    }

    public void setFechaCita(Date fechaCita) {
        this.fechaCita = fechaCita;
    }
    
    public LocalTime getHoraCita() {
        return horaCita;
    }

    public void setHoraCita(LocalTime horaCita) {
        this.horaCita = horaCita;
    }

    public int getPacienteId() {
        return pacienteId;
    }

    public void setPacienteId (int pacienteId){
        this.pacienteId = pacienteId;
    }
    
    public Cita() {
    }

    public Cita(Date fechaCita, int doctorId, int pacienteId) {
        this.fechaCita = fechaCita;
        this.doctorId = doctorId;
        this.pacienteId = pacienteId;
    }
    
    @Override
    public String toString() {
        return "Cita [codigoCita=" + codigoCita + ", doctorId=" + doctorId + ", fechaCita=" + fechaCita
                + ", pacienteId=" + pacienteId + "]";
    }

    
    
}
