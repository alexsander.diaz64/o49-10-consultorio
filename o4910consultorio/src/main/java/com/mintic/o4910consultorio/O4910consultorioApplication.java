package com.mintic.o4910consultorio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O4910consultorioApplication {

	public static void main(String[] args) {
		SpringApplication.run(O4910consultorioApplication.class, args);
	}

}
